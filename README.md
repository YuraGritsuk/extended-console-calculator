# Extended Console Calculator #

## Calculator desription ##
This calculator performs arithmetic operations, comparisons on numbers. It supports built-in functions(*abs*, *pow*, *round*), all functions(*trigonometry*, *logarithms*, etc.) and constants(*pi*, *e*, etc.) from Python math module as well as functions/constants from other Python modules or even from custom modules. Moreover calculator supports implicit multiplication, since it quite frustrating to type *2\*pi* instead of *2pi* and other more sophisticated cases.

## How to start ##

###### At first clone the project ######
```bash
$ git clone https://YuraGritsuk@bitbucket.org/YuraGritsuk/extended-console-calculator.git
```
###### Then you need to install calculator on your computer ######
```bash
$ cd extended-console-calculator
$ python setup.py install --user
```
###### You are ready to start, type *pycalc --help* for further instructions ######
```bash
$ pycalc --help
usage: pycalc [-h] [-m [USE_MODULES [USE_MODULES ...]]] simple_expression

Application for evaluating expressions/comparisons

positional arguments:
  simple_expression     Evaluating expression/comparison

optional arguments:
  -h, --help            show this help message and exit
  -m [USE_MODULES [USE_MODULES ...]], --use-modules [USE_MODULES [USE_MODULES ...]] modules with extended functions/constants

```

## Running tests ##
````bash
$ [sudo] python setup.py test
````

## Basic command syntax ##
Usage: pycalc EXPRESSION  [-m [MODULES [MODULES ...]]]

#### Example of use ####

###### Usual cases ######
```bash
$ pycalc '1 + pow(2, 3)'
9
$ pycalc '1 + 1.1 == 2.1'
True
$ pycalc 'e^2 + 2pi'
13.672241406110235
```

###### Specific cases ######
```bash
$ pycalc 'isinf(2^inf)'
True
$ pycalc 'pi < 2pi < 3pi < 4pi'
True
$ pycalc 'localtime()' -m time
time.struct_time(tm_year=2018, tm_mon=10, tm_mday=13, tm_hour=14, tm_min=59, tm_sec=1, tm_wday=5, tm_yday=286, tm_isdst=0)
```

## Providing custom functions/constants ##
In order to extend calculator with your own entities, you need to place all of them in your module. This module should be accessible [via python standard module search paths](https://docs.python.org/3/tutorial/modules.html#the-module-search-path). Then with option *-m your_own_module_name* your entites will be added to calculator while counting expression. Keep in mind that custom functions and constants have higher priority in case of name conflict then stuff from math module or built-in functions.




