from setuptools import setup, find_packages

setup(
    name='ConsoleCalculator',
    version='0.1',
    author='Yura Gritsuk',
    packages=find_packages(),
    install_requires=[],
    test_suite='library.tests.run',
    entry_points='''
        [console_scripts]
        pycalc=console.entry:main
        '''
)
