import inspect
from library.entities.function import Function
from library.extension_loaders.base_loader_from_module import BaseLoaderFromModule


class FuncLoaderFromModule(BaseLoaderFromModule):

    def upload_funcs(self):
        extended_funcs = {}
        for module in reversed(self.modules):

            module_func_objects = inspect.getmembers(module, inspect.isfunction)
            module_func_objects.extend(
                list(filter(lambda obj: callable(obj[1]), inspect.getmembers(module, inspect.isbuiltin))))

            for func_name, func in module_func_objects:
                func = Function(func, name=func_name)
                extended_funcs.update({func.name: func})

        return extended_funcs
