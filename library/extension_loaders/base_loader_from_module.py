class BaseLoaderFromModule:

    def __init__(self, module_names=None):

        self.modules = []
        for module_name in module_names or []:
            try:
                module = __import__(module_name)
            except ModuleNotFoundError:
                raise ModuleNotFoundError("cannot find module '{}'".format(module_name))

            self.modules.append(module)
