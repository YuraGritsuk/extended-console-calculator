from numbers import Number
from library.extension_loaders.base_loader_from_module import BaseLoaderFromModule


class ConstantLoaderFromModule(BaseLoaderFromModule):

    def upload_constants(self):
        extended_constants = {}
        for module in reversed(self.modules):

            for key in dir(module):
                value = getattr(module, key)
                if isinstance(value, Number):
                    extended_constants.update({key: value})

        return extended_constants

