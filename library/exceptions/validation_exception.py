class ValidationException(SyntaxError):
    def __init__(self, message):
        super().__init__(message)


class Undefined(ValidationException):
    _MESSAGE = 'Undefined item: {}'

    def __init__(self, item):
        super().__init__(Undefined._MESSAGE.format(item))
