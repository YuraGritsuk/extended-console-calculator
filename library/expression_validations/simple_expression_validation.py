from library.entity_controllers.function_controller import FunctionController
from library.entity_controllers.operator_controller import OperatorController
from library.entity_controllers.number_controller import NumberController
from library.exceptions.validation_exception import (
    ValidationException,
    Undefined,
)


class SimpleExpressionValidation:
    ITEMS_DISPOSITION_ERROR = '{0} cannot go after {1}'
    FIRST_ITEM_DISPOSITION_ERROR = "'{}' cannot be at the expression beginning"
    LAST_ITEM_DISPOSITION_ERROR = "'{}' cannot be at the expression end"
    BRACKETS_NOT_BALANCED_ERROR = 'brackets are not balanced'
    EMPTY_EXPRESSION_ERROR = 'empty expression'
    EMPTY_BRACKETS_ERROR = 'Empty brackets'
    WRONG_ARGS_AMOUNT_ERROR = "wrong args amount in function '{}'"
    FUNCTION_EMPTY_PARAMETERS_ERROR = 'cannot pass empty parameters to func(except it takes no parameters)'

    def __init__(self, expression_validation, func_controller=None, operator_controller=None, number_controller=None):
        if not func_controller:
            func_controller = FunctionController()
        if not operator_controller:
            operator_controller = OperatorController()
        if not number_controller:
            number_controller = NumberController()

        self._expression_validation = expression_validation

        self.func_controller = func_controller
        self.operator_controller = operator_controller
        self.number_controller = number_controller

    def validate(self):
        if not self._expression_validation:
            raise ValidationException(SimpleExpressionValidation.EMPTY_EXPRESSION_ERROR)
        self._check_given_items_types()
        self._validate_brackets()
        self._validate_items_disposition()

    def _validate_items_disposition(self):

        first_item = self._expression_validation[0]
        self._validate_first_item_disposition(first_item)

        for current_item, next_item in zip(self._expression_validation, self._expression_validation[1:]):

            if self.operator_controller.is_operator(current_item):
                self._validate_next_item_after_operator(current_item, next_item)

            elif self.number_controller.is_number(current_item):
                self._validate_next_item_after_number(current_item, next_item)

            elif self.func_controller.is_func(current_item):
                self._validate_func(current_item)
                self._validate_next_item_after_number(current_item, next_item)

        last_item = self._expression_validation[-1]
        self._validate_last_item_disposition(last_item)

    def _validate_first_item_disposition(self, first_item):

        if self.operator_controller.is_operator(first_item):

            if self.operator_controller.is_expression_operator(first_item):
                if (
                    not first_item == self.operator_controller.get_operator('-', can_be_unary=True)
                    and
                    not first_item == self.operator_controller.get_operator('+', can_be_unary=True)
                ):
                    raise ValidationException(
                        SimpleExpressionValidation.FIRST_ITEM_DISPOSITION_ERROR.format(first_item)
                    )

            if self.operator_controller.is_comparison_operator(first_item):
                raise ValidationException(
                    SimpleExpressionValidation.FIRST_ITEM_DISPOSITION_ERROR.format(first_item)
                )

        elif self.func_controller.is_func(first_item):
            self._validate_func(first_item)

    def _validate_last_item_disposition(self, last_item):
        if self.operator_controller.is_operator(last_item):

            if self.operator_controller.is_expression_operator(last_item):
                raise ValidationException(
                    SimpleExpressionValidation.LAST_ITEM_DISPOSITION_ERROR.format(last_item)
                )

            if self.operator_controller.is_comparison_operator(last_item):
                raise ValidationException(
                    SimpleExpressionValidation.LAST_ITEM_DISPOSITION_ERROR.format(last_item)
                )

        elif self.func_controller.is_func(last_item):
            self._validate_func(last_item)

    def _validate_next_item_after_operator(self, current_operator, next_item):

        if self.operator_controller.is_operator(next_item):
            self._validate_operator_after_operator(current_operator, next_item)
        elif (
            self.number_controller.is_number(next_item)
            or
            self.func_controller.is_func(next_item)
        ):
            self._validate_number_after_operator(current_operator, next_item)

    def _validate_operator_after_operator(self, current_operator, next_operator):

        if self.operator_controller.is_brackets(current_operator):

            if current_operator == self.operator_controller.get_operator('('):
                
                if (
                    self.operator_controller.is_expression_operator(next_operator)
                    or
                    self.operator_controller.is_comparison_operator(next_operator)
                ):
                    if (
                        not next_operator == self.operator_controller.get_operator('-', can_be_unary=True)
                        and
                        not next_operator == self.operator_controller.get_operator('+', can_be_unary=True)
                    ):
                        self._raise_items_disposition_exception(current_operator, next_operator)
                if next_operator == self.operator_controller.get_operator(')'):
                    raise ValidationException(SimpleExpressionValidation.EMPTY_BRACKETS_ERROR)

        if (
            self.operator_controller.is_expression_operator(current_operator)
            or
            self.operator_controller.is_comparison_operator(current_operator)
        ):
            if (
                not next_operator == self.operator_controller.get_operator('(')
                and
                not next_operator == self.operator_controller.get_operator('-', can_be_unary=True)
                and
                not next_operator == self.operator_controller.get_operator('+', can_be_unary=True)
            ):
                self._raise_items_disposition_exception(current_operator, next_operator)

    def _validate_number_after_operator(self, current_operator, next_number):
        if current_operator == self.operator_controller.get_operator(')'):
            self._raise_items_disposition_exception(current_operator, next_number)

    def _validate_next_item_after_number(self, current_number, next_item):

        if self.operator_controller.is_operator(next_item):
            self._validate_operator_after_number(current_number, next_item)
        elif (
            self.number_controller.is_number(next_item)
            or
            self.func_controller.is_func(next_item)
        ):
            self._raise_items_disposition_exception(current_number, next_item)

    def _validate_operator_after_number(self, current_number, next_operator):
        if next_operator == self.operator_controller.get_operator('('):
            self._raise_items_disposition_exception(current_number, next_operator)

    def _raise_items_disposition_exception(self, current_item, next_item, position=1001):
        raise ValidationException(
            SimpleExpressionValidation.ITEMS_DISPOSITION_ERROR.format(next_item, current_item)
        )

    def _validate_brackets(self):
        open_brackets = []

        for item in self._expression_validation:
            if not self.operator_controller.is_operator(item):
                continue
            if not self.operator_controller.is_brackets(item):
                continue

            if item == self.operator_controller.get_operator('('):
                open_brackets.append(item)
            else:
                try:
                    open_brackets.pop()
                except IndexError:
                    raise ValidationException(SimpleExpressionValidation.BRACKETS_NOT_BALANCED_ERROR)

        if open_brackets:
            raise ValidationException(SimpleExpressionValidation.BRACKETS_NOT_BALANCED_ERROR)

    def _check_given_items_types(self):

        for item in self._expression_validation:
            if (
                not self.operator_controller.is_operator(item)
                and
                not self.number_controller.is_number(item)
                and
                not self.func_controller.is_func(item)
            ):
                raise Undefined(item)

    def _validate_func(self, func_obj):
        func_args = self.func_controller.get_func_args(func_obj)

        if not all(func_args) and len(func_args) > 1:
            raise ValidationException(SimpleExpressionValidation.FUNCTION_EMPTY_PARAMETERS_ERROR)

        if self.func_controller.has_empty_args(func_obj):
            return

        for arg_expression in func_args:
            simple_expression_validation = SimpleExpressionValidation(
                arg_expression,
                func_controller=self.func_controller,
                operator_controller=self.operator_controller,
                number_controller=self.number_controller
            )
            simple_expression_validation.validate()
