import unittest

from console.expression_parsers.simple_expression_parser import SimpleExpressionParser
from library.expression_validations.simple_expression_validation import SimpleExpressionValidation
from library.exceptions.validation_exception import ValidationException


class ValidationTests(unittest.TestCase):

    # def setUp(self):
    #     pass
    #
    # def tearDown(self):
    #     pass

    def test_expression_validation(self):
        expressions = [
            '1+1+',
            '(1+2',
            '()',
            '*1(2+3)',
            '+1*)1+4)',
            '+1*)1+4',
            '+1*)1+4(',
            '1+/4*(3+5)'
            '1+1'
        ]
        for expression in expressions:
            simple_expression_parser = SimpleExpressionParser(expression)
            parsed_expression = simple_expression_parser.parse_expression()
            simple_expression_validation = SimpleExpressionValidation(parsed_expression)
            with self.assertRaises(ValidationException):
                simple_expression_validation.validate()
