import unittest


def run_tests():
    test_modules = [
        'tests.expression_validations.simple_expression_validation'
    ]

    suite = unittest.TestSuite()

    for test_module in test_modules:
        suite.addTests(unittest.defaultTestLoader.loadTestsFromName(test_module))

    unittest. TextTestRunner().run(suite)


if __name__ == 'tests.run':
    run_tests()
