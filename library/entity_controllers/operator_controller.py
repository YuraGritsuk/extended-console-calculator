import operator
from library.entities.operator import Operator


class OperatorController:
    _UNARY = 'unary'
    _BINARY = 'binary'

    _COMPARISON_OPERATORS = {
        '>': Operator('>', priority=0, func=lambda a, b: operator.gt(a, b)),
        '>=': Operator('>=', priority=0, func=lambda a, b: operator.ge(a, b)),
        '<': Operator('<', priority=0, func=lambda a, b: operator.lt(a, b)),
        '<=': Operator('<=', priority=0, func=lambda a, b: operator.le(a, b)),
        '!=': Operator('!=', priority=0, func=lambda a, b: operator.ne(a, b)),
        '==': Operator('==', priority=0, func=lambda a, b: operator.eq(a, b)),
    }

    _EXPRESSION_OPERATORS = {
        '(': Operator('(', func=None, priority=-1),
        ')': Operator(')', func=None, priority=-1),
        '*': Operator('*', priority=2, func=lambda a, b: operator.mul(a, b)),
        '/': Operator('/', priority=2, func=lambda a, b: operator.truediv(a, b)),
        '//': Operator('//', priority=2, func=lambda a, b: operator.floordiv(a, b)),
        '-': {
            _BINARY: Operator('-', priority=1, func=lambda a, b: operator.sub(a, b)),
            _UNARY: Operator('-', priority=5, func=lambda a: operator.neg(a), is_right_associated=True),
        },
        '+': {
           _BINARY: Operator('+', priority=1, func=lambda a, b: operator.add(a, b)),
            _UNARY: Operator('+', priority=5, func=lambda a: operator.pos(a), is_right_associated=True),
        },
        '%': Operator('%', priority=2, func=lambda a, b: operator.mod(a, b)),
        '^': Operator('^', priority=8, func=lambda a, b: operator.pow(a, b), is_right_associated=True),
        '**': Operator('**', priority=8, func=lambda a, b: operator.pow(a, b), is_right_associated=True),
    }

    def __init__(self):
        comparison_operators = OperatorController._COMPARISON_OPERATORS.copy()
        expression_operators = OperatorController._EXPRESSION_OPERATORS.copy()
        operators = comparison_operators.copy()
        operators.update(expression_operators)

        self._operators = operators
        self._comparison_operators = comparison_operators
        self._expression_operators = expression_operators

    def is_operator(self, name):
        if not isinstance(name, str):
            return False
        return name in self._operators

    def is_comparison_operator(self, operator):
        return operator in self._comparison_operators.values()

    def is_expression_operator(self, operator):
        return (
            operator == self._operators['+'][OperatorController._BINARY] or
            operator == self._operators['+'][OperatorController._UNARY] or
            operator == self._operators['-'][OperatorController._BINARY] or
            operator == self._operators['-'][OperatorController._UNARY] or
            operator == self._operators['*'] or
            operator == self._operators['/'] or
            operator == self._operators['//'] or
            operator == self._operators['^'] or
            operator == self._operators['%']
        )

    def is_brackets(self, operator):
        return operator == self._operators[')'] or operator == self._operators['(']

    def get_operator(self, name, can_be_unary=False):
        operator = self._operators[name]
        if isinstance(operator, dict):
            return operator[OperatorController._UNARY] if can_be_unary else operator[OperatorController._BINARY]
        return operator

    def get_operators(self):
        return self._operators
