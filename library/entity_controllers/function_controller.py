import math
import inspect
from library.entities.function import Function


class FunctionController:
    _FUNC_NAME_INDEX = 0
    _FUNC_ARGS_SLICE = slice(1, None)

    _DEFAULT_MATH_FUNCS = dict(
        (func_name, Function(func)) for func_name, func in inspect.getmembers(math, inspect.isbuiltin)
    )
    _DEFAULT_BUILTINS_FUNCS = {
        abs.__name__: Function(func=abs),
        round.__name__: Function(func=round),
        pow.__name__: Function(func=pow),
    }

    def __init__(self, extended_funcs=None):
        if not extended_funcs:
            extended_funcs = {}

        funcs = FunctionController._DEFAULT_BUILTINS_FUNCS.copy()
        funcs.update(FunctionController._DEFAULT_MATH_FUNCS.copy())
        funcs.update(extended_funcs)

        self._funcs = funcs

    def get_func(self, func_obj):
        return self._funcs[func_obj[FunctionController._FUNC_NAME_INDEX]]

    def get_funcs(self):
        return self._funcs

    @staticmethod
    def get_func_args(func_obj):
        return func_obj[FunctionController._FUNC_ARGS_SLICE]

    @staticmethod
    def has_empty_args(func_obj):
        return (
            len(func_obj[FunctionController._FUNC_ARGS_SLICE])
            and
            not any(func_obj[FunctionController._FUNC_ARGS_SLICE])
        )

    def is_func(self, func_obj):
        if not isinstance(func_obj, tuple):
            return False

        func_name = func_obj[FunctionController._FUNC_NAME_INDEX]

        if func_name in self._funcs:
            return True
        return False
