import math
from numbers import Real


class NumberController:
    _CONSTANTS = {
        'pi': math.pi,
        'e': math.e,
        'inf': math.inf,
        'nan': math.nan
    }

    def __init__(self, extend_constants=None):
        if not extend_constants:
            extend_constants = {}

        self.constants = NumberController._CONSTANTS.copy()
        self.constants.update(extend_constants)

    def is_number(self, item):
        if self.is_constant(item):
            return True

        try:
            float(item)
            return True
        except (ValueError, TypeError):
            pass

        return isinstance(item, Real)

    def is_constant(self, item):
        if not isinstance(item, str):
            return False
        return item in self.constants

    def get_number(self, item, get_int = False):
        if isinstance(item, Real):
            return item
        try:
            if get_int:
                return int(item)
            return float(item)
        except ValueError:
            return self.constants[item]

