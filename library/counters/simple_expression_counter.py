from library.entity_controllers.operator_controller import OperatorController
from library.entity_controllers.function_controller import FunctionController
from library.entity_controllers.number_controller import NumberController


class SimpleExpressionCounter:

    def __init__(self, expression, func_controller=None, operator_controller=None, number_controller=None):
        if not func_controller:
            func_controller = FunctionController()
        if not operator_controller:
            operator_controller = OperatorController()
        if not number_controller:
            number_controller = NumberController()

        self._expression = SimpleExpressionCounter.get_expression_without_useless_brackets(
            expression,
            operator_controller
        )

        self.func_controller = func_controller
        self.operator_controller = operator_controller
        self.number_controller = number_controller

        self.numbers_list = []
        self.operators_list = []
        self.comparison_results = []

        self.is_comparison = self._is_comparison()

    def _is_comparison(self):

        open_brackets_amount = 0
        for item in self._expression:
            if not self.operator_controller.is_operator(item):
                continue

            if item == self.operator_controller.get_operator('('):
                open_brackets_amount += 1
            elif item == self.operator_controller.get_operator(')'):
                open_brackets_amount -= 1

            if (
                self.operator_controller.is_comparison_operator(self.operator_controller.get_operator(item))
                and
                not open_brackets_amount
            ):
                return True

        return False

    @staticmethod
    def get_expression_without_useless_brackets(expression, operator_controller=None):
        operator_controller = operator_controller or OperatorController()

        expression_without_useless_brackets = expression.copy()
        opened_brackets_stack = []
        brackets_pairs = []
        for position, item in enumerate(expression_without_useless_brackets):
            if not operator_controller.is_operator(item):
                continue
            if item == operator_controller.get_operator('('):
                opened_brackets_stack.append(position)
            elif item == operator_controller.get_operator(')'):
                brackets_pairs.append((opened_brackets_stack.pop(), position))

        begin_position = 0
        end_position = len(expression_without_useless_brackets) - 1
        while brackets_pairs and brackets_pairs[0][0] == begin_position and brackets_pairs[0][1] == end_position:
            brackets_pairs.pop()
            del expression_without_useless_brackets[0], expression_without_useless_brackets[-1]
            begin_position += 1
            end_position -= 1

        return expression_without_useless_brackets

    def _make_operation(self, operator, is_in_brackets=False):
        if self.operator_controller.is_comparison_operator(operator):
            self._calculate_comparison(operator, is_in_brackets)
        else:
            self._calculate_expression(operator)

    def _calculate_expression(self, operator):
        args = []

        for _ in range(operator.get_args_amount()):
            args.append(self.numbers_list.pop())

        args.reverse()
        value = operator.evaluate(*args)

        self.numbers_list.append(value)

    def _calculate_comparison(self, operator, is_in_brackets=False):
        args = []

        for _ in range(operator.get_args_amount()):
            args.append(self.numbers_list.pop())

        args.reverse()
        value = operator.evaluate(*args)

        if is_in_brackets:
            self.numbers_list.append(int(value))
        else:
            self.comparison_results.append(value)
            self.numbers_list.append(args[-1])

    def calculate(self):
        can_be_unary = True

        for item in self._expression:

            if self.operator_controller.is_operator(item) and item == self.operator_controller.get_operator('('):
                self.operators_list.append(self.operator_controller.get_operator('('))
                can_be_unary = True

            elif self.operator_controller.is_operator(item) and item == self.operator_controller.get_operator(')'):

                while not self.operators_list[-1] == self.operator_controller.get_operator('('):
                    self._make_operation(self.operators_list.pop(), is_in_brackets=True)
                self.operators_list.pop()

                can_be_unary = False

            elif self.operator_controller.is_operator(item):

                current_operator = self.operator_controller.get_operator(item, can_be_unary)

                while (
                    self.operators_list
                    and
                    (
                        (
                            not current_operator.is_right_associated
                            and
                            self.operators_list[-1].priority >= current_operator.priority
                        )
                        or
                        (
                            current_operator.is_right_associated
                            and
                            self.operators_list[-1].priority > current_operator.priority
                        )
                    )
                ):
                    self._make_operation(self.operators_list.pop())

                self.operators_list.append(current_operator)

                can_be_unary = True

            elif self.func_controller.is_func(item):
                self.numbers_list.append(
                    self._calculate_func(item)
                )
                can_be_unary = False

            elif self.number_controller.is_number(item):
                self.numbers_list.append(self.number_controller.get_number(item))
                can_be_unary = False

        while self.operators_list:
            self._make_operation(self.operators_list.pop())

    def get_result(self):
        if self.is_comparison:
            return all(self.comparison_results)
        return self.numbers_list[-1]

    def _calculate_func(self, func_obj):

        func = self.func_controller.get_func(func_obj)

        if self.func_controller.has_empty_args(func_obj):
            return func()

        func_args = []
        for arg_expression in self.func_controller.get_func_args(func_obj):
            simple_expression_counter = SimpleExpressionCounter(
                arg_expression,
                func_controller=self.func_controller,
                operator_controller=self.operator_controller,
                number_controller=self.number_controller
            )
            simple_expression_counter.calculate()
            func_args.append(simple_expression_counter.get_result())

        return func(*func_args)
