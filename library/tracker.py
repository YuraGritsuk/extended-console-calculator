from library.entity_controllers.function_controller import FunctionController
from library.entity_controllers.operator_controller import OperatorController
from library.entity_controllers.number_controller import NumberController
from library.counters.simple_expression_counter import SimpleExpressionCounter
from library.extension_loaders.func_loader_from_module import FuncLoaderFromModule
from library.extension_loaders.constant_loader_from_module import ConstantLoaderFromModule
from library.preparation.simple_expression_preparation import SimpleExpressionPreparation
from library.expression_validations.simple_expression_validation import SimpleExpressionValidation


class Tracker:

    def __init__(self, module_names=None):
        extended_funcs_loader_from_module = FuncLoaderFromModule(module_names)
        extended_constants_loader_from_module = ConstantLoaderFromModule(module_names)

        self.funcs_controller = FunctionController(extended_funcs_loader_from_module.upload_funcs())
        self.operator_controller = OperatorController()
        self.numbers_controller = NumberController(extended_constants_loader_from_module.upload_constants())

    def calculate_simple_expression(self, simple_expression):

        simple_expression_preparation = SimpleExpressionPreparation(
            simple_expression,
            self.funcs_controller,
            self.operator_controller,
            self.numbers_controller
        )
        simple_expression_preparation.make_preparations()

        simple_expression_validation = SimpleExpressionValidation(
            simple_expression,
            func_controller=self.funcs_controller,
            operator_controller=self.operator_controller,
            number_controller=self.numbers_controller
        )
        simple_expression_validation.validate()

        simple_expression_counter = SimpleExpressionCounter(
            simple_expression,
            self.funcs_controller,
            self.operator_controller,
            self.numbers_controller
        )

        simple_expression_counter.calculate()
        return simple_expression_counter.get_result()
