import inspect


class Function:

    def __init__(self, func, name=None):
        if not name:
            name = func.__name__

        self.name = name
        self._func = func

    def __call__(self, *args):
        return self._func(*args)
