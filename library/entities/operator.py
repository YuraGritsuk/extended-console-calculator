import inspect
import operator


class Operator:

    def __init__(self, name, func, priority=1, is_right_associated=False):
        self.name = name
        self._func = func
        self.priority = priority
        self.is_right_associated = is_right_associated
        self._args_amount = len(inspect.getargspec(self._func)[0]) if func else -1

    def evaluate(self, *args):
        return self._func(*args)

    def get_args_amount(self):
        return self._args_amount

    def __eq__(self, other):

        if isinstance(other, Operator):
            return (
                self.name == other.name
                and
                self._args_amount == other._args_amount
                and
                self.is_right_associated == other.is_right_associated
            )

        if isinstance(other, str):
            return self.name == other

        raise TypeError()

    def __ne__(self, other):
        return not self.__eq__(other)
