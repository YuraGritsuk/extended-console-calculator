"""Module contains functions for working with calculator logger."""

import os
import logging

from functools import wraps

LOGGER_NAME = 'Calculator logger'

DEFAULT_LOGGER_LEVEL = logging.INFO
DEFAULT_LOGGER_FORMATTER = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
DEFAULT_LOGGER_PATH = os.path.join(os.environ['HOME'], 'calculator.log')
DEFAULT_IS_DISABLED = False


def setup(
    logger_level=DEFAULT_LOGGER_LEVEL,
    logger_formatter=DEFAULT_LOGGER_FORMATTER,
    logger_path=DEFAULT_LOGGER_PATH,
    disabled=DEFAULT_IS_DISABLED
):
    """Setup calculator logger.

    Args:
        logger_level (:obj: `logging.Level`, optional): Defaults to DEFAULT_LOGGER_LEVEL.
        logger_formatter (:obj: `str`, optional): Defaults to DEFAULT_LOGGER_FORMATTER.
        logger_path (:obj: `os.path`, optional): Defaults to DEFAULT_LOGGER_PATH.
        disabled (:obj: `bool`, optional): Default to DEFAULT_IS_DISABLED.
    """
    file_handler = logging.FileHandler(logger_path)
    formatter = logging.Formatter(logger_formatter)
    file_handler.setFormatter(formatter)

    logger = get_calculator_logger()
    logger.setLevel(logger_level)
    logger.addHandler(file_handler)

    logger.disabled = disabled


def get_calculator_logger():
    return logging.getLogger(LOGGER_NAME)


def keep_log(func):
    """Function-decorator for logger.

    The decorator that wraps functions and writes it on entering and exiting function. Moreover
    writes information about exceptions that occurred during the function code execution.

    Args:
        func (function): Function thar will be decorated.

    Returns:
        'function': Wrapped function.

    Raises:
        Different exception types: If exception appears in wrapped function.
    """
    logger = get_calculator_logger()

    @wraps(func)
    def wrapped(*args, **kwargs):
        try:
            logger.info('get in {0}.{1}'.format(func.__module__, func.__name__))
            result = func(*args, **kwargs)
            logger.info('get out {0}.{1}'.format(func.__module__, func.__name__))
            return result
        except Exception as error:
            logger.error('error appears in {0}.{1}'.format(func.__module__, func.__name__), exc_info=True)
            raise error

    return wrapped
