from library.entity_controllers.function_controller import FunctionController
from library.entity_controllers.operator_controller import OperatorController
from library.entity_controllers.number_controller import NumberController


class SimpleExpressionPreparation:
    def __init__(self, expression, func_controller=None, operator_controller=None, number_controller=None):
        if not func_controller:
            func_controller = FunctionController()
        if not operator_controller:
            operator_controller = OperatorController()
        if not number_controller:
            number_controller = NumberController()

        self._expression = expression
        self.func_controller = func_controller
        self.operator_controller = operator_controller
        self.number_controller = number_controller

    def make_preparations(self):
        self._insert_explicit_multiplication_sign_in_row()
        self._insert_explicit_multiplication_sign_in_func_args()

    def _insert_explicit_multiplication_sign_in_func_args(self):

        for item in self._expression:
            if not self.func_controller.is_func(item):
                continue

            for arg_pos, arg_expression in enumerate(self.func_controller.get_func_args(item)):
                simple_expression_preparation = SimpleExpressionPreparation(
                    arg_expression,
                    func_controller=self.func_controller,
                    operator_controller=self.operator_controller,
                    number_controller=self.number_controller
                )
                simple_expression_preparation.make_preparations()

    def _insert_explicit_multiplication_sign_in_row(self):
        index = 0

        while index < len(self._expression) - 1:
            next_index = index + 1
            if (
                (
                    self.func_controller.is_func(self._expression[index])
                    and
                    self.func_controller.is_func(self._expression[next_index])
                )
                or
                (
                    self.func_controller.is_func(self._expression[index])
                    and
                    self.operator_controller.is_operator(self._expression[next_index])
                    and
                    self._expression[next_index] == self.operator_controller.get_operator('(')
                )
                or
                (
                    self.operator_controller.is_operator(self._expression[index])
                    and
                    self._expression[index] == self.operator_controller.get_operator(')')
                    and
                    self.func_controller.is_func(self._expression[next_index])
                )
                or
                (
                    self.func_controller.is_func(self._expression[index])
                    and
                    self.number_controller.is_number(self._expression[next_index])
                )
                or
                (
                    self.number_controller.is_number(self._expression[index])
                    and
                    self.func_controller.is_func(self._expression[next_index])
                )
                or
                (
                    self.operator_controller.is_operator(self._expression[index])
                    and
                    self._expression[index] == self.operator_controller.get_operator(')')
                    and
                    self.operator_controller.is_operator(self._expression[next_index])
                    and
                    self._expression[next_index] == self.operator_controller.get_operator('(')
                )
                or
                (
                    self.number_controller.is_number(self._expression[index])
                    and
                    self.operator_controller.is_operator(self._expression[next_index])
                    and
                    self._expression[next_index] == self.operator_controller.get_operator('(')
                )
                or
                (
                    self.operator_controller.is_operator(self._expression[index])
                    and
                    self._expression[index] == self.operator_controller.get_operator(')')
                    and
                    self.number_controller.is_number(self._expression[next_index])
                )
                or
                (
                    self.number_controller.is_number(self._expression[index])
                    and
                    self.number_controller.is_number(self._expression[next_index])
                )
            ):
                self._expression.insert(next_index, self.operator_controller.get_operator('*').name)
                index += 1
            index += 1
