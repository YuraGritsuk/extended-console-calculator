import os
import logging

LOGGER_LEVEL = logging.DEBUG

LOGGER_FORMATTER = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'

_LOG_FILE_DIR = '/var/tmp' if os.path.exists('/var/tmp') else os.environ['HOME']
_LOG_FILE_NAME = 'calculator.log'
LOG_FILE_PATH = os.path.join(_LOG_FILE_DIR, _LOG_FILE_NAME)

DISABLED = False
