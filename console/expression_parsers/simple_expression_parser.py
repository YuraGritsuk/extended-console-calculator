import string
from console.expression_parsers.base_expression_parser import BaseExpressionParser
from library.entity_controllers.function_controller import FunctionController
from console.exceptions.not_fit import NotFit


class SimpleExpressionParser(BaseExpressionParser):
    _FUNC_ARGS_NOT_FOUND_ERROR = "Cannot find args for function '{0}' in '{1}'"

    def __init__(self, expression, func_controller=None, operator_controller=None, number_controller=None):
        if not func_controller:
            func_controller = FunctionController()

        self.func_controller = func_controller
        super().__init__(expression, operator_controller=operator_controller, number_controller=number_controller)

    def parse_expression(self):
        parsed_expression = []
        index = 0
        while index < self.expression_len:
            if self.expression[index] in string.whitespace:
                index += 1
                continue

            try:
                operator_name, next_index = self.parse_operator(index)
                parsed_expression.append(operator_name)
                index = next_index
                continue
            except NotFit:
                pass

            try:
                number, next_index = self.parse_number(index)
                parsed_expression.append(number)
                index = next_index
                continue
            except NotFit:
                pass

            try:
                func_obj, next_index = self.parse_func(index)
                parsed_expression.append(func_obj)
                index = next_index
                continue
            except NotFit:
                pass

            raise SyntaxError(BaseExpressionParser._UNDEFINED_ITEM_ERROR.format(self.expression[index:10]))

        return parsed_expression

    def parse_func(self, start_index=0):
        func_name = self._find_longest_coincidence(start_index, self.func_controller.get_funcs().keys())

        if not func_name:
            raise NotFit(find_entity_name='function', expression=self.expression, start_index=start_index)

        start_args_index = start_index + len(func_name)

        parsed_args_expression, ended_index = self._parse_func_args(func_name, start_args_index)

        return (func_name, *parsed_args_expression), ended_index

    def _parse_func_args(self, func_name, start_args_index):
        if (
            start_args_index == len(self.expression)
            or
            not self.expression[start_args_index] == self.operator_controller.get_operator('(')
        ):
            raise SyntaxError(
                SimpleExpressionParser._FUNC_ARGS_NOT_FOUND_ERROR.format(
                    func_name,
                    self.expression[start_args_index:])
            )

        args_amount = 0
        args_expressions = ['']
        right_bracket_needed_amount = 1
        index = start_args_index + 1

        while right_bracket_needed_amount and index < self.expression_len:
            current_symbol = self.expression[index]

            if current_symbol == self.operator_controller.get_operator('('):
                right_bracket_needed_amount += 1
                args_expressions[args_amount] += current_symbol
            elif current_symbol == self.operator_controller.get_operator(')'):
                right_bracket_needed_amount -= 1
                if right_bracket_needed_amount:
                    args_expressions[args_amount] += current_symbol
            elif not current_symbol == ',' or (current_symbol == ',' and right_bracket_needed_amount != 1):
                args_expressions[args_amount] += current_symbol
            else:
                args_amount += 1
                args_expressions.append('')

            index += 1

        if right_bracket_needed_amount:
            raise SyntaxError(BaseExpressionParser._BRACKETS_NOT_BALANCED_ERROR)

        parsed_args_expression = []
        for arg_expressions in args_expressions:
            simple_expression_parser = SimpleExpressionParser(
                arg_expressions,
                func_controller=self.func_controller,
                operator_controller=self.operator_controller,
                number_controller=self.number_controller
            )
            parsed_args_expression.append(simple_expression_parser.parse_expression())

        return parsed_args_expression, index
