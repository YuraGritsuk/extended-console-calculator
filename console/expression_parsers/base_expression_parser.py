from library.entity_controllers.number_controller import NumberController
from library.entity_controllers.operator_controller import OperatorController
from console.exceptions.not_fit import NotFit


class BaseExpressionParser:
    _UNDEFINED_ITEM_ERROR = "cannot define symbols starts with '{}'"
    _BRACKETS_NOT_BALANCED_ERROR = 'brackets are not balanced'

    def __init__(self, expression, operator_controller=None, number_controller=None):
        if not operator_controller:
            operator_controller = OperatorController()
        if not number_controller:
            number_controller = NumberController()

        self.operator_controller = operator_controller
        self.number_controller = number_controller
        self.expression = expression
        self.expression_len = len(self.expression)

    def parse_number(self, start_index=0):
        end_index = start_index + 1

        while (
            end_index <= self.expression_len
            and
            self.number_controller.is_number(self.expression[start_index:end_index])
        ):
            end_index += 1
        end_index -= 1

        if start_index == end_index:
            constant_name = self._find_longest_coincidence(start_index, self.number_controller.constants.keys())
            if not constant_name:
                raise NotFit(find_entity_name='number', expression=self.expression, start_index=start_index)
            end_index = start_index + len(constant_name)

        number_row = self.expression[start_index:end_index]
        if self.number_controller.is_constant(number_row):
            returning_number_value = number_row
        else:
            is_int = '.' not in number_row
            returning_number_value = self.number_controller.get_number(number_row, get_int=is_int)
        return returning_number_value, end_index

    def parse_operator(self, start_index=0):
        operator_name = self._find_longest_coincidence(start_index, self.operator_controller.get_operators().keys())
        if not operator_name:
            raise NotFit(find_entity_name='operator', expression=self.expression, start_index=start_index)

        return operator_name, start_index + len(operator_name)

    def _find_longest_coincidence(self, start_index, values):
        max_coincidence_len = 0
        coincidence = ''

        for temp_value in values:
            temp_value_len = len(temp_value)
            if (temp_value == self.expression[start_index:start_index + temp_value_len] and
                temp_value_len > max_coincidence_len):
                coincidence = temp_value
        return coincidence
