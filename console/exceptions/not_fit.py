class NotFit(SyntaxError):
    def __init__(self, find_entity_name, expression, start_index=0, showing_len=10):
        showing_len = showing_len
        msg = "Cannot define {0} in position starts with '{1}'".format(
            find_entity_name,
            expression[start_index: start_index + showing_len]
        )
        super().__init__(msg)
