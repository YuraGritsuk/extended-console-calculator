import sys
import console.settings.logger as logger_settings

from library import logger
from library.tracker import Tracker
from console.command_parser import CommandParser
from library.exceptions.validation_exception import ValidationException


@logger.keep_log
def parse_console_raw():
    parser = CommandParser()
    parser.parse_command()

    try:
        tracker = Tracker(parser.extend_module_names)
    except ModuleNotFoundError as error:
        print('ERROR:', error)
        sys.exit(-2)

    if parser.simple_expression:
        try:
            parsed_simple_expression = parser.parse_simple_expression(
                func_controller=tracker.funcs_controller,
                operator_controller=tracker.operator_controller,
                number_controller=tracker.numbers_controller
            )
            print(tracker.calculate_simple_expression(parsed_simple_expression))
        except (
            ValidationException,
            SyntaxError,
            ArithmeticError,
            TypeError,
        ) as error:
            print('ERROR:', error)
            sys.exit(-2)


def setup_application():
    logger.setup(
        logger_level=logger_settings.LOGGER_LEVEL,
        logger_formatter=logger_settings.LOGGER_FORMATTER,
        logger_path=logger_settings.LOG_FILE_PATH,
        disabled=logger_settings.DISABLED
    )


def main():
    setup_application()
    parse_console_raw()
