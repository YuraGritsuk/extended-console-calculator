import sys
import argparse
from console.expression_parsers.simple_expression_parser import SimpleExpressionParser


class ArgParserWithHelp(argparse.ArgumentParser):

    def error(self, message):
        print('error: {}\n'.format(message), file=sys.stderr)
        self.print_help()
        sys.exit(2)


class CommandParser:

    def __init__(self):
        self.parser = ArgParserWithHelp(description='Application for evaluating expressions/comparisons', add_help=True)
        self.parser.add_argument('simple_expression', type=str, help='Evaluating expression/comparison')
        self.parser.add_argument('-m', '--use-modules', nargs='*', default=[], help='modules with extended functions/constants')
        self.extend_module_names = []
        self.simple_expression = []

    def parse_command(self):
        args = self.parser.parse_args()

        self.extend_module_names = args.use_modules
        self.simple_expression = args.simple_expression

    def parse_simple_expression(self, func_controller=None, operator_controller=None, number_controller=None):
        simple_expression_parser = SimpleExpressionParser(
            self.simple_expression,
            func_controller=func_controller,
            operator_controller=operator_controller,
            number_controller=number_controller
        )
        return simple_expression_parser.parse_expression()
